# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 ] ]

SUMMARY="Text widget with syntax highlighting support"
HOMEPAGE="http://www.gnome.org/"

LICENCES="LGPL-2.1"
SLOT="2"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="doc glade [[ description = [ build and install glade catalog ] ]]"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40]
        virtual/pkg-config[>=0.20]
        doc? ( dev-doc/gtk-doc[>=1.11] )
    build+run:
        dev-libs/glib:2[>=2.16]
        dev-libs/libxml2[>=2.5]
        x11-libs/gtk+:2[>=2.12]
        glade? ( dev-util/glade[>=3.2] )
"

RESTRICT="test" # requires X

# drops gnomevfs dep
DEFAULT_SRC_CONFIGURE_PARAMS=( '--disable-build-test' '--enable-deprecations' )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'doc gtk-doc' 'glade glade-catalog' )

src_prepare() {
    # avoid gnome-common dep
    edo sed -i -e '/GNOME_COMPILE_WARNINGS/d' "${WORK}"/configure.ac

    # need to re-run libtoolize to install locales in the correct location
    autotools_src_prepare
}

