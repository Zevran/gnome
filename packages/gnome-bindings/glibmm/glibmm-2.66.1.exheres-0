# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gnome.org [ suffix=tar.xz ] meson

SUMMARY="C++ bindings for glib"
HOMEPAGE="https://www.gtkmm.org"

LICENCES="LGPL-2.1"
SLOT="2.4"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    doc
    disable-deprecated [[ description = [ Omit deprecated API from the library ] ]]
"

UPSTREAM_DOCUMENTATION="
    https://developer.gnome.org/glibmm/stable/ [[
        lang = en
        description = [ Reference manual ]
    ]]
"

DEPENDENCIES="
    build:
        dev-lang/python:*[>=3.5]
        virtual/pkg-config[>=0.20]
        doc? (
            app-doc/doxygen
            dev-lang/perl:*[>=5.6.0]
            dev-libs/libxslt
            media-gfx/graphviz
        )
    build+run:
        dev-cpp/libsigc++:2[>=2.9.1]
        dev-libs/glib:2[>=2.61.2]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dbuild-examples=false
)
MESON_SRC_CONFIGURE_OPTION_SWITCHES=(
    'doc build-documentation'
    '!disable-deprecated build-deprecated-api'
)

src_prepare() {
    meson_src_prepare

    # Needs internet access (DNS)
     edo sed -e '/giomm_tls_client/d' -i tests/meson.build

    # Fix docdir
    edo sed \
        -e "/install_docdir/s:/ book_name:/ '${PNVR}':" \
        -i docs/reference/meson.build
}

