# Copyright 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gnome.org [ suffix=tar.xz ] gtk-icon-cache
require python [ blacklist=none ] meson

SUMMARY="A GObject plugins library"
HOMEPAGE="http://live.gnome.org/Libpeas"

LICENCES="LGPL-2.1"
SLOT="1.0"
PLATFORMS="~amd64 ~x86"

FILTERED_PYTHON3_ABIS="${PYTHON_FILTERED_ABIS/2.7 }"

MYOPTIONS="
    gtk-doc
    python_abis: ( ${FILTERED_PYTHON3_ABIS} ) [[ number-selected = at-most-one ]]
    ( linguas: an ar as ast be bg bn_IN ca ca@valencia cs da de el en_GB eo es et eu fa fi fr fur gl
               gu he hi hu id it ja kn ko lt lv ml mr nb nds nl or pa pl pt pt_BR ro ru sk sl sr
               sr@latin sv ta te tg th tr ug uk vi zh_CN zh_HK zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.11] )
    build+run:
        dev-libs/glib:2[>=2.38.0]
        gnome-bindings/pygobject:3[>=3.2.0][python_abis:*(-)?]
        gnome-desktop/gobject-introspection:1[>=1.39.0]
        x11-libs/gtk+:3[>=3.0.0][gobject-introspection]
"

RESTRICT="test" # requires X

MESON_SRC_CONFIGURE_PARAMS=(
    '-Ddemos=false'
    '-Dglade_catalog=false'
    '-Dintrospection=true'
    '-Dlua51=false'
    '-Dvapi=false'
    '-Dwidgetry=true'
)

MESON_SRC_CONFIGURE_OPTION_SWITCHES=( 'gtk-doc gtk_doc' )

src_configure() {
    PYTHON_OPTIONS=()
    if optionq python_abis:2.7; then
        PYTHON_OPTIONS+=( -Dpython2=true )
    else
        PYTHON_OPTIONS+=( -Dpython2=false )
    fi

    for so in ${FILTERED_PYTHON3_ABIS}; do
        optionq python_abis:$so && PYTHON3_SO=$so
    done
    if [[ -n $PYTHON3_SO ]]; then
        PYTHON_OPTIONS+=( -Dpython3=true )
    else
        PYTHON_OPTIONS+=( -Dpython3=false)
    fi
    meson_src_configure "${PYTHON_OPTIONS[@]}"
}

